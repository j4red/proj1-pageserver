### Identifying Information ###

**Author:** Jared Knofczynski, jknofczy@uoregon.edu  
**Description:** My implementation of Project 1 for CIS 322. Serves HTML and CSS
over a simple web server implemented via Python.

### Running The Program ###
Ensure the `credentials.ini` file is placed in the `/pageserver/` directory. Once
installed, deployment should work "out of the box" with this command sequence:

  ```
  git clone git@bitbucket.org:j4red/proj1-pageserver.git <targetDirectory>
  ```

  ```
  cd proj1-pageserver
  ```

  ```
  make run or make start
  ```

  ```
  make stop
  ```

While the server is running, test it using the `tests.sh` script located within
the `/tests/` directory. Usage:

 ```
 ./tests.sh localhost:PORT
 ```

Where PORT is the port the web server is running on.
